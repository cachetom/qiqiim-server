var websocketurl="ws://"+window.location.hostname+":2048/ws";   //ws://{ip}:{端口}/{java后端websocket配置的上下文}

var reconnectflag = false;//避免重复连接
var socket; 

function createWebSocket(url,callbak) {
   try { 
   	// webscoket protocol 不同于http protocol 点在于 http protocol 没法由服务端主动向客户端推送消息。
   	//而 webscoket 就是来解决这个问题的方案。而且webscoket 同样可以由 客户端向服务端推送消息。
   	// webscoket protocol is html5 new protocol
   	// 详细介绍见：https://www.cnblogs.com/zxtceq/p/6963964.html
   	/**
   	 * 实现Web实时应用的技术出现了，WebSocket通过浏览器提供的API真正实现了具备像C/S架构下的桌面系统的实时通讯能 力。
   	 * 其原理是使用JavaScript调用浏览器的API发出一个WebSocket请求至服务器，经过一次握手，和服务器建立了TCP通讯，
   	 * 因为它本质 上是一个TCP连接，所以数据传输的稳定性强和数据传输量比较小。
   	 */
   	
   	// 官方介绍翻译地址：https://blog.csdn.net/u013252773/article/details/24186013
   	// 创建scoket 对象 并触发回调函数
      if (!window.WebSocket) {
  	      window.WebSocket = window.MozWebSocket; 
  	  }  
  	  if (window.WebSocket) {
  		socket = new WebSocket(url);
        socket.binaryType = "arraybuffer"; 
        callbak();
  	  } else {
         // layer.msg("你的浏览器不支持websocket！");
  	     //当浏览器不支持websocket时 降级为http模式	  
  	    var isClose =false;
  	    
  	    
  	    // onbeforeunload：https://www.w3cschool.cn/fetch_api/fetch_api-9vhu2oq0.html
  		window.onbeforeunload =function() {
  			if(!isClose){
  				return "确定要离开当前聊天吗?";
  			}else{
  				return "";
  			}
  		}
  		//当用户关闭一个页面时触发 onunload 事件。
  		window.onunload =function() {
  			if(!isClose){
  				Imwebserver.closeconnect(); 
  			}
  		} 


		dwr.engine.setActiveReverseAjax(true);
  	    dwr.engine.setNotifyServerOnPageUnload(true);
  	    dwr.engine.setErrorHandler(function(){  
  	    });
  	    dwr.engine._errorHandler = function(message, ex) {
  	       //alert("服务器出现错误");
  	       //dwr.engine._debug("Error: " + ex.name + ", " + ex.message, true);
  	    };
  	    Imwebserver.serverconnect();
  		  
      }  
    } catch (e) { 
       reconnect(url,callbak);
    }     
}
 





// 断线重连
function reconnect(url,callbak) {
	//reconnectflag 默认是false 表示掉线状态。
	// 下面这种判断方式值得学习使用，思路更简单，（你若在线即（reconnectflag = true） 则把你踢滚蛋。不让你继续执行）
    if(reconnectflag) return;
    
    reconnectflag = true;
    
    //没连接上会一直重连，设置延迟避免请求过多
    // setTimeout 在指定的毫秒数后调用函数或计算表达式。
    setTimeout(function () {
        createWebSocket(url,callbak);
        reconnectflag = false;
    }, 2000);
}

 
 

